// Using DOM
// Retrieve an element from webpage
const names = document.querySelectorAll('.name');
const spanFullname = document.querySelector('#span-full-name');

/*Alternative in retrieving an element: getElement
	document.getElementById('txt-first-name');
	document.getElementsByClassName('txt-inputs');
	document.getElementsByName('input')*/


/*// Event listener: interaction between user and the web page
x.addEventListener('keyup', (event) => {
	spanFullname.innerHTML = (x[0] + x[1]);
})

// Assign same event to multiple listeners

x.addEventListener('keyup', (event) => {
	// Contains the element where the event happened
	console.log(event.target);
	// Gets the value of the input object
	console.log(event.target.value);
})
*/

let fullNamer = ('keyup', (event) => {
	spanFullname.innerHTML = names[0] + " " + names[1];
})

fullNamer()